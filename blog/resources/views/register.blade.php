<!DOCTYPE html>
<html>
<head>
	<title>Membuat Web Statis dengan Laravel</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form action="/welcome" methode="POST">
		<!--First Name & Last Name Input Type Text-->
		<label for="first_name">First Name:</label><br>
		<input type="text" name="first_name" id="first_name"><br>
		<label for="last_name">Last Name:</label><br>
		<input type="text" name="last_name" id="last_name"><br><br>
		<!--Gender Input Type Radio-->
		<label>Gender:</label><br>
		<input type="radio" name="gender" id="male" value="Male">
		<label for="male">Male</label><br>
		<input type="radio" name="gender" id="female" value="Female">
		<label for="female">Female</label><br>
		<input type="radio" name="gender" id="other" value="Other">
		<label for="other">Other</label><br><br>
		<!-- Nationality-->
		<label for="nationality">Nationality</label><br>
		<select id="nationality" name="nationality">
			<option value="Indonesia">Indonesia</option>
			<option value="Singapore">Singapore</option>
			<option value="Japan">Japan</option>
			<option value="Korea">Korea</option>
			<option value="Brazil">Brazil</option>
			<option value="Other">Other</option>
		</select><br><br>
		<!-- Language Checkbox-->
		<label>Language Spoken:</label><br>
		<input type="checkbox" name="language" id="indonesia" value="Bahasa Indonesia">
		<label for="indonesia">Bahasa Indonesia</label><br>
		<input type="checkbox" name="language" id="english" value="English">
		<label for="english">Englsih</label><br>
		<input type="checkbox" name="language" id="other" value="Other">
		<label for="other">Other</label><br><br>
		<!-- Bio Textarea -->
		<label for="bio">Bio:</label><br>
		<textarea id="bio" rows="7" name="bio"></textarea><br>
		<input type="submit" name="submit" value="Sign Up">
	</form>
</body>
</html>